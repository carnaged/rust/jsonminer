Features Planned
================
- [x] Load JSON File
- [x] CLI argument parsing and call subfunctions
- [x] Output json as greppable, using an xpath like syntax for each value
- [ ] Output parsed json for an expression
    > Expression in this scope referes to an xpath or xpath-like syntax for JsonMiner which is being worked on
- [ ] Aggregation Expressions: join of multiple expressions...
- [ ] Basic math operations on top of aggregations
  - [ ] Sum
  - [ ] Count
  - [ ] Avg
  - [ ] Min/Max

Optional Features
=================
- [ ] Concat multiple json files. (maybe as an array)
- [ ] Load directory structure as json
- [ ] Drop into a console to load json once and play with expressions
- [ ] MongoDB like aggregation syntax, or use it as inspiration for additional features
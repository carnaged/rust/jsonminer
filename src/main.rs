#![warn(missing_debug_implementations, missing_docs, rust_2018_idioms)]

//! A cli json parser for quick and efficient pattern searching.
//!
//! JsonMiner is intended for quick parsing of json for data mining in large files
//! It is useful for quick recon from web apis, database results and research works.

use json;

use json::JsonValue;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

/// Keeps track of the current function carried out by the miner
enum Command {
    Load(String),
}

/// Parses arguments and builds the [Command][comm] enum
///
/// [comm]: crate::Command
fn parse_args(args: Vec<String>) -> Command {
    match args.get(1).get_or_insert(&String::default()).as_str() {
        "load" => {
            // let filepath = args.get(2).get_or_insert(&String::default()).to_string();
            let filepath = args.get(2).expect("No filepath provided").to_string();
            Command::Load(filepath)
        }
        _ => panic!("Oh no. Command not provided"),
    }
}

/// Opens the file for parsing
fn print_file(filepath: String) {
    let path = Path::new(&filepath);
    let mut f = File::open(path).expect("Couldnt open file");
    let mut s = String::new();
    f.read_to_string(&mut s).expect("Couldnt read from file");
    // println!("{}", s);
    let jsonvalue = json::parse(s.as_str()).expect("Invalid json");
    // println!("{:#?}", jsonvalue);
    jsonline_printer("$".to_string(), &jsonvalue);
}

/// Parses the file and prints to stdout
fn jsonline_printer(header: String, o: &JsonValue) {
    match o {
        JsonValue::Array(_) => o
            .members()
            .enumerate()
            .for_each(|(i, v)| jsonline_printer(format!("{}[{}]", header, i.to_string()), &v)),
        JsonValue::Object(_) => o
            .entries()
            .for_each(|(k, v)| jsonline_printer(format!("{}.{}", header, k.to_string()), &v)),
        JsonValue::Null => println!("{}#: Null", header),
        JsonValue::Short(s) => println!("{}#: {}", header, s),
        JsonValue::String(s) => println!("{}#: {}", header, s),
        JsonValue::Number(n) => println!("{}#: {}", header, n),
        JsonValue::Boolean(b) => println!("{}#: {}", header, b),
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let command: Command = parse_args(args);

    match command {
        Command::Load(filepath) => print_file(filepath),
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(1 + 1, 2);
    }
}
